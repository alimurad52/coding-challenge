/**
 * Please find a path to rescue the dog (d) from the maze
 * The maze is a 2 dimensional array which path is ' ' (space) and blocks are '#'
 * you can move left, right, top, up
 *
 * return path should be the array of position numbers order from start to reach dog.
 * 
 * what is the position number?
 * It is the number we assign for each cell in the matrix from left to right and top to bottom in an incremented value.
 * which start from 0 to (size of the matrix - 1)
 *
 * example for calculating position
 * matrix size 8 x 6 (row x column)
 * a[0][1] = 1
 * a[1][1] = 7
 * a[7][5] = 47
 *
 * If you cannot find the path please return undefined.
 *
 * See the test if you have questions.
 */
    // ['#', ' ', '#', '#', '#', '#'],
    // ['#', ' ', ' ', '#', '#', '#'],
    // ['#', '#', ' ', '#', '#', '#'],
    // ['#', '#', ' ', '#', '#', '#'],
    // ['#', '#', ' ', ' ', '#', '#'],
    // ['#', '#', '#', ' ', ' ', ' '],
    // ['#', '#', '#', '#', '#', ' '],
    // ['#', '#', '#', '#', '#', 'd'],

    // [1, 7, 8, 14, 20, 26, 27, 33, 34, 35, 41, 47]


export const rescuePrincessPath = ({rows, columns, startPosition, maze}) => {

    let map = new Map();
    let endPosition = {};

    for(let i = 0; i < rows; i++) {
        let result = [];
        for(let k = 0; k < columns; k++) {
            if(maze[i][k] === ' ') {
                //push all empty position of row
                result.push({y: k, p: (i*columns)+k})
            }
            if(i === (rows-1)) {
                if(maze[i][k] === 'd') {
                    //set the exit point
                    endPosition = {x:i, y:k};
                }
            }
        }
        // push each set of array to a map to have the empty positions as a set for each row
        map.set(i, result);
    }
    let arr = [];
    const getResult = (map, rowId, endPoint, column) => {
        if(endPoint.x === 0) {
            return arr;
        }
        console.log(rowId, endPoint);
        const el = map.get(rowId),
            topEl = map.get(rowId-1),
            bottomEl = map.get(rowId+1);
        if(el.length === 0 || (el.length === 1 && el[0].y === endPoint.y)) {
            // console.log(arr, endPoint, rowId);
            let found = topEl.find(e => e.y === endPoint.y);
            if(found) {
                arr.push(found);
                return getResult(map, rowId-1, {x: rowId-1, y: found.y}, column);
            } else {
                let found = bottomEl.find(e => e.y === endPoint.y);
                arr.push(found);
                return getResult(map, rowId-1, {x: rowId-1, y: found.y}, column);
            }
        } else {
            // console.log(topEl, endPoint, rowId);
            let found = topEl.find(e => e.y === endPoint.y);
            if(found) {
                arr.push(found);
                return getResult(map, rowId-1, {x: rowId-1, y: found.y}, column)
            } else {
                if(endPoint.y === column) {
                    let found = el.find(e => e.y === endPoint.y-1);
                    // console.log(el);
                    arr.push(found);
                    return getResult(map, rowId, {x: rowId, y: found.y}, column);
                } else {
                    if(arr.length === 0 ) {
                        let found = el.find(e => e.y === (endPoint.y+1));
                        if(found) {
                            arr.push(found);
                            return getResult(map, rowId, {x: rowId, y: found.y}, column);
                        } else {
                            let found2 = el.find(e => e.y === (endPoint.y-1));
                            arr.push(found2);
                            return getResult(map, rowId, {x: rowId, y: found2.y}, column);
                        }
                    } else {
                        let lastEl = arr[arr.length-1];
                        // console.log(endPoint, bottomEl);
                        let found = el.find(e => e.y === (endPoint.y+1));
                        if(lastEl && (found && found.y === lastEl.y)) {
                            console.log("hit");
                        } else {
                            let found = el.find(e => e.y === (endPoint.y-1));
                            let found2 = el.find(e => e.y === (endPoint.y+1));
                            // console.log(found2, el, rowId, endPoint);
                            if(lastEl && (found && found.y !== endPoint.y)) {
                                arr.push(found);
                                return getResult(map, rowId, {x: rowId, y: found.y}, column);
                            } else if(lastEl && (found2 && found2.y !== endPoint.y)) {
                                arr.push(found2);
                                return getResult(map, rowId, {x: rowId, y: found2.y}, column);
                            } else {
                                if(bottomEl && bottomEl.length !== 0) {
                                    let found = bottomEl.find(e => e.y === endPoint.y);
                                    arr.push(found);
                                    return getResult(map, rowId+1, {x: rowId+1, y: found.y}, column);
                                } else {
                                    let found = topEl.find(e => e.y === endPoint.y);
                                    arr.push(found);
                                    return getResult(map, rowId-1, {x: rowId-1, y: found.y}, column);
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    let result = getResult( map, rows-1, endPosition, columns-1);
    result.unshift({y: endPosition.y, p: endPosition.y+((rows-1)*columns)});
    console.log(result);
    return result.map((item) => {
        return item.p
    }).reverse();
};
