/**
 * Export a function named 'count'.
 * It should call the given callback every 10th of a second with a increment from given start to end integer
 * Return a function to prematurely cancel the counting process.
 *
 * @param {number} start - Given to callback first
 * @param {number} end - Stop when count reached this value
 * @param {function(number): void} callback - Called after each 100ms with an increment
 * @returns {function(): void} - Cancel countdown function
 */

export const count = ({startCount, endCount, callback, cancelAt}) => {
    let counter = 0;
    let timer = setInterval(function(){
        counter++;
        console.log(counter*100)
        if((counter*100) === (cancelAt*1000)) {
            console.log("hit");
            return true;
        }
        if((counter*100) >= (startCount*1000) && counter <= (endCount*1000)) {
            clearInterval(timer);
            return callback;
        }  else {
            clearInterval(timer);
            return true;
            // clearInterval(timer);
        }
    }, 100);
};
