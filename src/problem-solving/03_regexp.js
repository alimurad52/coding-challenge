/**
 * This test, tests some knowledge of Regular Expressions.
 *
 * See the test for what functions you need to implement.
 */
module.exports.containsNumber = (str) => {
    // [0-9] looks for any numbers in the string
    let reg = new RegExp(/[0-9]/);
    return reg.test(str);
};

module.exports.containsRepeatingLetter = (str) => {
    // [a-zA-Z] looks for all matches
    // \1 looks for a match of same char in the previous group
    // ? to make the serach greedy so it looks for all possible matches
    let reg = new RegExp(/([a-zA-Z]).?\1/);
    return reg.test(str);
};

module.exports.endsWithVowel = (str) => {
    // $ checks the last char of the str against [AEIOUaeiou]
    let reg = new RegExp(/[AEIOUaeiou]$/);
    return reg.test(str);
};

module.exports.captureThreeNumbers = (str) => {
    let reg = new RegExp(/([1-9]){0,3}/g);
    console.log(str.match(reg));
};

module.exports.matchesPattern = (str) => {
    // ^ ensures to start the check from the beginning of the string
    // $ marks the end of the matched string
    // \d detects all digits
    // \d{3}- ensures that the length of the digits is 3 and that right after is a -
    let reg = new RegExp(/^\d{3}-\d{3}-\d{4}$/);
    return reg.test(str);
};

module.exports.isUSD = (str) => {
    // ^[$] ensure the first character is a $
    // \d{1,3} ensures max length of digits is 3
    // (?:,\d{3}) looks for an optional comma with trailing 3 digits
    // *(?:\.\d{2})?$/)
    let reg = new RegExp(/^[$]\d{1,3}(?:,\d{3})*(?:\.\d{2})?$/);
    return reg.test(str);
};
