/**
 * Get the last n data elements from the nested object
 *
 * See the test if you have questions
 */
export const getLastNumbers = (payload, n) => {
    const arr = [];
    // recursive function to retrieve all the data values
    function data(payload) {
        arr.push(payload.data);
        // if payload next does not exist that means it is the last value
        if(!payload.next) {
            return arr;
        } else {
            // return the function with the next, next dictionary
            return data(payload.next)
        }
    }

    const tempArr = data(payload);
    // if the request last numbers are greater than the retrieved data numbers return all data numbers
    if(n >= tempArr.length) return tempArr;
    // to splice out the last requested numbers
    return tempArr.splice(tempArr.length-n, tempArr.length-1);
};
