/**
 * Have a look at the test and implement the needed function, so the test will succeed
 */

export const sum = (arr) => {
    let sum = 0;
    for(let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum;
    //with the reduce function of es5
    // return arr.reduce((a, b) => a+b);
};

export const concat = (...args) => {
    const result = [];
    for(let k = 0; k < args.length; k++) {
        const arr = args[k];
        for(let i = 0; i < arr.length; i++) {
            result.push(arr[i]);
        }
    }
    return result;
    //with es5 reduce and concat
    // return args.reduce((a, b) => a.concat(b));
};

export const count = (arr, n) => {
    let count = 0;
    for(let i = 0; i < arr.length; i++) {
        if(n === arr[i]) count++;
    }
    return count;
    //with es5 filter function
    // return arr.filter(a => a === n).length;
};

export const duplicates = (arr) => {
    let obj = {};
    const duplicates = [];
    for(let i = 0; i < arr.length; i++) {
        const val = arr[i];
        if(obj[val]) {
            if(!duplicates.includes(val)) duplicates.push(val);
            obj[val] = obj[val] + 1;
        } else {
            obj[val] = 1;
        }
    }
    return duplicates;
};

export const square = (arr) => {
    let results = [];
    for(let i = 0; i < arr.length; i++) {
        results.push(arr[i]*arr[i]);
    }
    return results;
    //with es5 map function
    // return arr.map(a => a*a);
};
