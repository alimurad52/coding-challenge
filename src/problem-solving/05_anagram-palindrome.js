/**
 * Check if the given string is an anagram of a palindrom
 *
 * Example
 * s = 'aabbccdd'
 * One way this can be arranged into a palindrome is abcddcba. Return true.
 *
 * Constraints
 * contains only lowercase letters in the range ascii[a..z]
 */
export const isPalindromePossible = (str) => {
    // if the length is 2 or less than 2 palindrome is not possible
    if(str.length <= 2) return false;

    let palindromeSum = 0;
    const allLetters = {};
    // using dictionary to record number of occurances
    for (let i = 0; i < str.length; i++) {
        const l = str[i];
        // if letter does not exist in dictionary set it to 0
        if(!allLetters[l]) {
            allLetters[l] = 0;
        }
        // of each occurance increase the count
        allLetters[l]++;
    }
    // if remainder is 0 then palindrome is possible
    for (let lCount in allLetters) {
        if(allLetters[lCount] % 2) {
            palindromeSum++;
        }
    }
    // if length is greater than 1 palindrome is not possible
    return palindromeSum <= 1 ;
};
